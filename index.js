

//REPETITIVE STRUCTURES -------------
//---------------------------------------------- LOOPS

////---------------------------------------------- WHILE Loop

// While Loop
/*
	Do-while loops guarantee that the code will be excuted at least once
	Do-while will run a statement before the condition
	Syntax:

		initialization

		while(condition) {
			statement
		} 

		Order of Execution: 
		1st - condition
		2nd - statement
		loob back to to check 1st - condition
*/
// let x = 0

// while ( x < 5) {
// 	console.log(++x);
// }

//Mini Activityy

// let x = 1

// while ( x <= 5) {
// 	console.log(x);
// 	x++;

// }


////---------------------------------------------- D0-WHILE Loop

// DO-While Loop
/*
	Do-while loops guarantee that the code will be excuted at least once
	Do-while will run a statement before the condition
	Syntax:

		initialization

		do {
			statement
		} while (condition/expression)

		Order of Execution: 
		1st - statement
		2nd - condition
		loob back to to check 1st - statement (if the condition is good)

*/

// let number = Number(prompt("Give me a number:"));

// do {
// 	console.log("Do While: " + number);
// 	number += 1
// } while(number < 10)


////---------------------------------------------- FOR Loop

// FOR Loop
/*

	Syntax:
		for (intialization; expression/condition: finalExpression) {
			statement
		}

		Order of Execution: 
		1. The init step is executed first, and only once. This step allows you to declare and initialize any loop control variables.
		2. Next, the condition is evaluated. ...
		3. After the body/statement of the 'for' loop executes, the flow of control jumps back up to the increment statement. ...
		The condition is now evaluated again.
*/

// for ( let count = 0; count <= 20; count++) {
// 	console.log("For Loop: " + count);
// }

// let myString = "alex";

// console.log(myString.length)
// console.log(myString[0]);
// console.log(myString[3]);

// console.log(`Alex has these letters`);

// for ( let x = 0; x < myString.length; x++) {
// 	console.log((myString[x]));
// }


// let myName = "Jeremiah";

// for (let i = 0; i < myName.length; i++) {

// 	if (
// 		myName[i].toLowerCase() === "a" ||
// 		myName[i].toLowerCase() === "e" ||
// 		myName[i].toLowerCase() === "i" ||
// 		myName[i].toLowerCase() === "o" ||
// 		myName[i].toLowerCase() === "u" 
// 		)
// 	{
// 	console.log("vowel")
// 		}
// 		else {
// 		console.log(myName[i])
// 	}
// 	}


//---------------------------------------------- FOR Loop
//FOR LOOP (Continue and Break Statement)
/*
	"break" statement is used to terminate the current loop once a match is found

	"continue" statement allows the code to go to the next iteration of the of the loop without finishing the execution of all statements in a code block

*/

// for (let count = 0; count <= 20; count++) {

// 	if (count % 2 === 0) {
// 		console.log("Even Number: " + count)
// 		continue;
// 	}

// 	console.log("Continue and Break: " + count)

// 	if (count > 10)
// 	{
// 		break;
// 	}
// }

// let name = "majoha";

// 	//let i = 0; 0 < 6; i++)
// for (let i = 0; i < name.length; i++) {

// 	console.log(name[i])

// 	if (name[i].toLowerCase() === "a") {
// 		console.log("Continue to next iteration");
// 		continue;
// 	}


// 	if (name[i] === "h"){
// 		break;
// 	}
// }

// Returns a random integer from 0 to 20:
// let guessNumber = Math.floor(Math.random() * 21);

// console.log(guessNumber);

// let limitOfTries = 10;

// for(try = 0; try < limitOfTries; try++) {
// 	let myGuess = Number(prompt(`Guess a number from 0 to 20:`))

// 			if (myGuess < guessNumber){
// 				alert(`You guessed ${myGuess}.
// 					Choose a higher number.`)
// 				continue;
// 			}
// 				console.log(`Your guess: ${myGuess}.`);

// 			if (guessNumber === myGuess) {
// 				alert("You got it! The number is " +guessNumber)
// 				console.log(`Correct guess: ${myGuess}.`);
// 				break;
// 			}

// }